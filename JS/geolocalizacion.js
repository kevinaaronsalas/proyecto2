obtDistrito();

function obtDistrito() {
    var webDistrito = 'https://ubicaciones.paginasweb.cr/provincia/2/canton/10/distritos.json'
    traerDatos(webDistrito);
}

function traerDatos(purl) {
    fetch(purl)
        .then((respuesta) => {
            return respuesta.json();
        }).then((respuesta) => {
            let resultado = document.querySelector('#distritos');
            let resultado2 = document.querySelector('#distritos2');
            for (var clave in respuesta) {
                if (respuesta.hasOwnProperty(clave)) {
                    resultado.innerHTML += "<option value='" + clave + "'>" + respuesta[clave] + "</option>";
                    resultado2.innerHTML += "<option value='" + clave + "'>" + respuesta[clave] + "</option>";
                }
            }

        })
}